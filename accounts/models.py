from django.db import models
from django.contrib.auth.models import AbstractUser,User
from django.utils import timezone
from django.shortcuts import reverse

class CustomUser(AbstractUser):
    """Updated user object with new params and methods

    Args:
        AbstractUser ([obj]): inherit the Abstractuser class

    """
    phone_number = models.CharField(max_length=11,unique=True,null=True,blank=True)
    birth_date = models.DateTimeField(blank=True,null=True)
    
    def __str__(self):
        """model objects displaying names

        Returns:
            [str]: user email
        """
        return self.email
    
    def get_absolute_url(self):
        return reverse('accounts:signin')
