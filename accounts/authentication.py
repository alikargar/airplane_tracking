from .models import CustomUser as User

class EmailAuthentication:
    ''' Authenticate user by email '''
    def authenticate(self,username=None,password=None):
        user=User.objects.filter(email=username)
        if user.exists() and user[0].check_password(password):
            return user[0]
        else:
            return None

    def get_user(self,user_id):
        try:
            return User.objects.get(pk= user_id)
        except User.DoesNotExist:
            return None
