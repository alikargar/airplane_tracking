from django import forms
from . import models

class Tracking(forms.ModelForm):
    """Aiplane Tracking adding form """
    class Meta:
        model = models.FlightUser
        fields = ("flight",'schedule_delay')
        widgets = {"flight":forms.Select()}
        help_texts = {"schedule_delay": "In minutes"}