from django.shortcuts import render,reverse
from django.views.generic import CreateView, ListView,DeleteView,View
from . import models
from . import forms
from django.contrib.auth.mixins import LoginRequiredMixin
from django.http import HttpResponseRedirect
from django.urls import reverse_lazy

class TrackingCreateView(LoginRequiredMixin,CreateView):
    """Create a tracking scheduler task"""
    form_class = forms.Tracking
    template_name  = "tracking/create.html"
    
    def form_valid(self,form):
        flight_user = models.FlightUser.objects.filter(user=self.request.user,flight = form.cleaned_data['flight'])
        if flight_user.exists():
            flight_user = flight_user.first()
            flight_user.status = True
            flight_user.schedule_delay = form.cleaned_data['schedule_delay']
            flight_user.save()
            return HttpResponseRedirect(reverse("tracking:list_tracking"))
        self.object=form.save(commit=False)
        self.object.user= self.request.user
        self.object.status = True
        return super(TrackingCreateView,self).form_valid(form)

class TrackingListView(LoginRequiredMixin,ListView):
    """List of tracking tasks"""
    template_name = "tracking/list.html"
    model = models.FlightUser
    context_object_name = "user_flights"
    
    def get_queryset(self,*args,**kwargs):
        
        return super(TrackingListView,self).get_queryset(*args,**kwargs).select_related("flight").filter(user=self.request.user).order_by('-status')

class DisableTrackingView(LoginRequiredMixin,View):
    """Disable tracking task """
    def get(self, request, *args, **kwargs):
        flight_status=models.FlightUser.objects.filter(pk=kwargs['pk'])
        if flight_status.exists():
            flight_status = flight_status.first()
            flight_status.status=False
            flight_status.save()
        return  HttpResponseRedirect(reverse("tracking:list_tracking"))

class EnableTrackingView(LoginRequiredMixin,View):
    """Disable tracking task """
    def get(self, request, *args, **kwargs):
        flight_status=models.FlightUser.objects.filter(pk=kwargs['pk'])
        if flight_status.exists():
            flight_status = flight_status.first()
            flight_status.status=True
            flight_status.save()
        return  HttpResponseRedirect(reverse("tracking:list_tracking"))
    
class TrackingDeleteView(LoginRequiredMixin,DeleteView):
    """Delete tracking task"""
    model = models.FlightUser
    success_url = reverse_lazy("tracking:list_tracking")