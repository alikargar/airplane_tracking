from celery import shared_task,Celery,task
from celery.schedules import crontab
import json
from . import models
from datetime import datetime
import pytz
from django.core.cache import cache
from django_celery_beat.models import PeriodicTask, IntervalSchedule
import json
import decimal
from math import sin, cos, sqrt, atan2, radians
from django.core.mail import send_mail


@shared_task(name="tracking")
def tracking(username,flight_number,user_email):
    """Main logic for check airplane status and notify user by changes of the status in the interval

    Args:
        username ([str]): username of own user
        flight_number ([str]): flight number of the flight
        user_email ([str]): email of the user
    """    
    # gets last flight status crawled from database
    current_flight_status = models.FlightStatus.objects.filter(flight__flight_number=flight_number).order_by("-date")
    if not current_flight_status.exists():
        return
    current_flight_status=current_flight_status.first()
    cacheKey = username+"-"+flight_number
    # Gets the last flight status sent to the user
    last_status_sent_pk = cache.get(cacheKey)
    # Checks if both current status and last status are the same
    if current_flight_status.pk == last_status_sent_pk:
        pt = PeriodicTask.objects.filter(name=flight_number)
        if pt.exists():
            # Check if crawling of the flight statuses done
            if pt.first().enabled == False:
                # Disable the user flight tracking scheduler
                disable_scheduler(cacheKey)
                flight_user = models.FlightUser.objects.filter(user__username = username,flight__flight_number = flight_number)
                if flight_user.exists():
                    flight_user=flight_user.first()
                    # Set the status of tracking to false
                    flight_user.status = False
                    flight_user.save()
    # Check if the last flight status that sent to user exists
    if isinstance(last_status_sent_pk,int):
        last_status_sent = models.FlightStatus.objects.filter(pk = last_status_sent_pk)
        if last_status_sent.exists():
            last_status_sent= last_status_sent.first()
            # Compare 2 status of flight ,last status sent and the current status of flight 
            comparator = Comparator(current_flight_status,last_status_sent)
            # Notify user if there is any changes
            if comparator.compare_arrival_time() or comparator.compare_distance():
                mail_user(username,comparator,user_email,False)
            # Set the current status as last status sent
            cache.set(cacheKey,current_flight_status.pk)
            return
    # Calculate some params of current flight status
    # Here is the first time that user starts tracking
    comparator = Comparator(current_flight_status,None)
    mail_user(username,comparator,user_email,True)
    # Set the current status as last status sent
    cache.set(cacheKey,current_flight_status.pk)
    
@task
def mail_user(username,comparator_obj,user_email,is_first):
    """Send email to user 

    Args:
        username ([str]): username of own user
        comparator_obj ([Object]): an instance of Comparator class
        user_email ([str]): email of own user
        is_first (bool): check if its first email that is sending to user or not
    """    
    if is_first:
        message = """
            Hi dear {0} \n  
            Last status of airplane {1} : \n
            Time of Arrival: {2} \n
            Current Location: {3}
            """.format(username,
                    comparator_obj.flight_number,
                    comparator_obj.arrival_time,
                    comparator_obj.current_location)
    else:
        message = """
            Hi dear {0} \n

            New update of airplane {1} status: \n 

            Time of Arrival: {2} ({3} hours delay)\n
            Location: changed from {4} to {5}
            """.format(
                username,
                comparator_obj.flight_number,
                comparator_obj.arrival_time,
                comparator_obj._find_arrival_time_delay(),
                comparator_obj.last_location,
                comparator_obj.current_location
            )

    send_mail("Package Tracking from Airplane monitoring App",
              message,
              "ali.kargar.p@gmail.com",
              (user_email,))
    
class Comparator(object):
    """Compare 2 status of the user's flight,extracts the formatted values of the objects
    """    
    def __init__(self,current_flight_status,last_flight_status):
        """ class instantiate

        Args:
            current_flight_status ([Object]): FlightStaus instance
            last_flight_status ([Object]): FlightStaus instance
        """        
        self.current_flight_status = current_flight_status
        self.last_flight_status = last_flight_status
    
    def compare_arrival_time(self):
        """Compares arrival time of flights

        Returns:
            [bool]: True if there is a differ between the objects and False otherwise
        """        
        if self.last_flight_status.arrival_time != self.current_flight_status.arrival_time:
            return True
        return False
    
    def compare_distance(self):
        """Compares distance between 2 statuses 

        Returns:
            [type]: True if distance is more than 200km, False otherwise
        """   
        if self._find_distance()>200:
            return True
        return False
    
    def _find_arrival_time_delay(self):
        """Find differ between arrival time of last status and current 

        Returns:
            [decimal]: the difference
        """        
        return "{:.2f}".format((self.current_flight_status.arrival_time - self.last_flight_status.arrival_time).total_seconds()/3600)
            
    def _find_distance(self):
        """Finding the distance of 2 statuses in km.

        Returns:
            [float]: distance
        """        
        R = 6373.0
        lat1 = radians(self.current_flight_status.latitude)
        lon1 = radians(self.current_flight_status.longitude)
        lat2 = radians(self.last_flight_status.latitude)
        lon2 = radians(self.last_flight_status.longitude)
        dlon = lon2 - lon1
        dlat = lat2 - lat1
        a = sin(dlat / 2)**2 + cos(lat1) * cos(lat2) * sin(dlon / 2)**2
        c = 2 * atan2(sqrt(a), sqrt(1 - a))
        return R * c
    
    @property
    def current_location(self):
        """Cuttrnt location of the flight in specific format

        Returns:
            [str]: current loc
        """        
        return str([str(self.current_flight_status.latitude),str(self.current_flight_status.longitude)])
    @property
    def last_location(self):
        """last location of the flight sent before in specific format
        Returns:
            [str]: last loc
        """        
        return str([str(self.last_flight_status.latitude),str(self.last_flight_status.longitude)])
        
    @property
    def flight_number(self):
        """Flight number of the flight 

        Returns:
            [type]: [description]
        """        
        return self.current_flight_status.flight.flight_number
    
    @property
    def arrival_time(self):
        """Time of arrival in specific fornat

        Returns:
            [type]: [description]
        """        
        return self.current_flight_status.arrival_time.strftime("%d %B %Y, %I:%M %p")
    
@shared_task(name='crawler')
def crawler(filepath,flight_number):
    """Crawler

    Args:
        filepath ([sre]): path of the file of crawling data 
        flight_number ([str]): flight number
    """    
    cleaned_data = dict()
    with open(filepath,mode="r") as f:
        position = 0
        # Get the last flight status t
        queryset = models.FlightStatus.objects.filter(flight__flight_number=flight_number).select_related("flight").order_by('-date')
        if queryset.exists():
            # calculate last crawled data
            position = queryset[0].position + 1
        json_obj = json.load(f)
        data_list= json_obj['data']
        # Check if the last crawled data is not the last crawl
        if position>data_list.__len__()-1:
            f.close()
            # Disable the crawler for this flile 
            disable_scheduler(flight_number)
            return 
        data = data_list[position]
        cleaned_data['position']=position
        cleaned_data['latitude'] = data['coordinates']['latitude']
        cleaned_data['longitude'] = data['coordinates']['longitude']
        cleaned_data['flight_id'] = models.Flight.objects.get(flight_number=flight_number).pk
        cleaned_data['arrival_time'] = convert_datetime(data['arrival_time'])
        models.FlightStatus.objects.create(**cleaned_data)
    return

def convert_datetime(timestamp):
    """Convert the timestamp to normal datetime object

    Args:
        timestamp ([int]):timestamp

    Returns:
        [obj]: datetime object
    """    
    local_tz = pytz.timezone("Asia/Tehran") 
    utc_dt = datetime.utcfromtimestamp(timestamp).replace(tzinfo=pytz.utc)
    return local_tz.normalize(utc_dt.astimezone(local_tz))


def disable_scheduler(name):
    """Disable the scheduled task by the name

    Args:
        name ([str]): name of PeriodicTask
    """    
    pt = PeriodicTask.objects.filter(name=name)
    if pt.exists():
        ps_obj = pt.first()
        # Disable the scheduler
        ps_obj.enabled = False
        ps_obj.save()
        
def update_or_create_scheduler(delay,name,task,kwargs):
    """update or create the PeriodicTask Shcedukler

    Args:
        delay ([int]): delay of the task for schedule
        name ([str]): name of PeriodicTask
        task ([str]): task name in str
        kwargs ([dict]): dictionary needed for the task
    """    
    # Get or Creates an interval
    schedule = IntervalSchedule.objects.filter(every=delay, period=IntervalSchedule.MINUTES)
    if schedule.exists():
        schedule = schedule.first()
    else:
        schedule = IntervalSchedule.objects.create(every=delay, period=IntervalSchedule.MINUTES)
    
    # Create or update the PeriodicTask for scheduling and activating
    try:
        periodic_task = PeriodicTask.objects.get(name=name)
        periodic_task.interval = schedule
        periodic_task.task = task
        periodic_task.kwargs = json.dumps(kwargs)
        periodic_task.enabled = True
        periodic_task.save()
    except PeriodicTask.DoesNotExist:
        periodic_task =PeriodicTask.objects.create(interval = schedule,name=name,task = task,kwargs = json.dumps(kwargs),enabled = True)

        

