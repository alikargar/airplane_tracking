from django.db import models
from django.conf import settings
from django.core.files.storage import FileSystemStorage
from accounts.models import CustomUser
from django.shortcuts import reverse
from django.core.validators import MinValueValidator
# Create your models here.

class Flight(models.Model):
    """Flights informations model
    """    
    origin = models.CharField(max_length=200)
    destination = models.CharField(max_length=200)
    flight_number = models.CharField(max_length=200,unique=True)
    airline = models.CharField(max_length=200)
    schedule_delay = models.PositiveIntegerField(default=1,validators=[MinValueValidator(1)])
    informations_file = models.FileField(storage=FileSystemStorage(location=settings.MEDIA_ROOT), 
                                         upload_to='flights_info',
                                         default='flights_info/crawler_result.json')
    users = models.ManyToManyField(CustomUser,through='FlightUser',related_name='flights',symmetrical=False)
    def __str__(self):
        return self.flight_number
    
class FlightStatusManager(models.Manager):
    """ Custom Flight Status manager """
    def create(self,*args,**kwargs):
        """FlightStatus create manager customized to count and save the position of object in the crawled objects
        """        
        if 'position' not in kwargs:
            queryset = self.filter(flight__flight_number=kwargs['flight_number']).order_by('-date')
            if queryset.exists():
                position = queryset[0].position + 1
                kwargs['position']=position
        return super(FlightStatusManager,self).create(*args,**kwargs)
    
class FlightStatus(models.Model):
    """Status of flight resulted by crawler
    """    
    flight = models.ForeignKey(Flight,on_delete=models.CASCADE)
    position = models.PositiveIntegerField(default=0)
    date = models.DateTimeField(auto_now_add=True)
    longitude = models.DecimalField(max_digits=6, decimal_places=3)
    latitude = models.DecimalField(max_digits=6, decimal_places=3)
    arrival_time = models.DateTimeField(auto_now_add=False)
    
    objects = FlightStatusManager()
    
    def __str__(self):
        return self.flight.flight_number + " - " + str(self.position)
    
class FlightUser(models.Model):
    """Flight's users through model to create connection between users and flights
    """    
    user = models.ForeignKey(CustomUser,related_name="user_flights",on_delete=models.CASCADE)
    flight = models.ForeignKey(Flight,related_name = "flight_users",on_delete=models.CASCADE)
    status = models.BooleanField(default=False)
    schedule_delay = models.PositiveIntegerField(default=1,validators=[MinValueValidator(1)])
    
    class Meta:
        unique_together = ("user","flight")
    
        
    def get_absolute_url(self):
        return reverse('tracking:list_tracking')

    def __str__(self):
        return self.user.username+","+self.flight.flight_number