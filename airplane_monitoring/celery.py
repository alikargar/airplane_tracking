import os
from celery import Celery

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'airplane_monitoring.settings')

app = Celery('airplane_monitoring')
app.config_from_object('django.conf:settings', namespace='CELERY')
app.autodiscover_tasks()